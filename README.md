Django simple_rating

Allows add rating mixin to any model.
Store values in separate table.

1.   Add to INSTALLED_APPS after all built-ins 'simple_rating'
2.   Add simple_rating/templates to TEMPLATE_DIRS

> python manage.py collectstatic

   will put all static to {{STATIC}}/sr