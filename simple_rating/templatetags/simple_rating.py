from django import template


class RateControlNode(template.Node):

    def __init__(self, user_rel, instance_rel):
        self.user_rel = user_rel
        self.instance_rel = instance_rel

    def render_for_anonymous(self, context):
        t = template.loader.get_template("rate_read_only.html")
        instance = context[self.instance_rel]
        return t.render(template.Context({
            "total_rate": instance.get_total_rate()
        }
        ))

    def render(self, context):
        user = context[self.user_rel]
        if user.is_anonymous():
            return self.render_for_anonymous(context)
        instance = context[self.instance_rel]
        rating_storage = instance.get_rating_storage_for(user)
        user_vote = rating_storage.value
        t = template.loader.get_template("rate_control.html")
        if user_vote == 0:
            plus_class, minus_class = "enable", "enable"
        elif user_vote == 1:
            plus_class, minus_class = "disable", "enable"
        else:
            plus_class, minus_class = "enable", "disable"
        app = instance.__class__._meta.app_label
        cls_name = instance.__class__.__name__
        return t.render(template.Context(
            {
                "vote_link": "/rate/{0}/{1}/{2}".format(app,
                                                        cls_name,
                                                        instance.id),
                "total_rate": instance.get_total_rate(),
                "user_vote": user_vote,
                "plus_class": plus_class,
                "minus_class": minus_class,
                "id": 1
            }
        ))


def display_rating(parser, token):
    parsed = token.split_contents()
    if len(parsed) != 3:
        raise template.TemplateSyntaxError(
            "%r tag requires a user argument and instance argument" %
            token.contents.split()[0])

    return RateControlNode(*parsed[1:])


register = template.Library()

register.tag("display_rating", display_rating)
