from django.db import models
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.dispatch import receiver
from django.db.models.signals import post_save


from simple_rating import fields as sm_fields


class RatingStorage(models.Model):

    user = models.ForeignKey(get_user_model())
    value = models.fields.IntegerField()

    def vote(self, rate):
        self.value = rate
        self.save()


class RatingCacheStorage(models.Model):
    pos_value = models.fields.IntegerField()
    neg_value = models.fields.IntegerField()

    def value(self):
        return self.pos_value + self.neg_value * (-1)


class SimpleRatingMixin(models.Model):

    class Meta:
        abstract = True

    rating = models.ManyToManyField(RatingStorage, blank=True, editable=False)
    rating_cache = models.OneToOneField(RatingCacheStorage,
                                        blank=True,
                                        null=True,
                                        default=None,
                                        editable=False)

    def vote(self, user, rate):
        storage = self.get_rating_storage_for(user)
        old_value = storage.value
        storage.vote(rate)
        rating_cache = self.get_rating_cache()

        if old_value < 0:
            rating_cache.neg_value -= 1
        elif old_value > 0:
            rating_cache.pos_value -= 1
        if rate < 0:
            rating_cache.neg_value += 1
        else:
            rating_cache.pos_value += 1

        rating_cache.save()

    def get_rating_storage_for(self, user):
        try:
            return self.rating.filter(user=user)[0]
        except (ObjectDoesNotExist, IndexError):
            storage = RatingStorage(user=user, value=0)
            storage.save()
            self.rating.add(storage)
            self.save()
            return storage

    def get_rating_cache(self):
        if self.rating_cache is None:
            rating_cache = RatingCacheStorage(pos_value=0, neg_value=0)
            rating_cache.save()
            self.rating_cache = rating_cache
            self.save()
        return self.rating_cache

    def get_total_rate(self):
        rating_cache = self.get_rating_cache()
        return rating_cache.value()

    def get_total_rate_naive(self):
        return sum((rate.value for rate in self.rating.all()))
