from django.conf.urls import patterns, include, url

#^(?P<username>\w+)/blog/'

urlpatterns = \
    patterns('',
             # Examples:
             url(r'^(?P<appname>\w+)/(?P<model_name>\w+)/(?P<id>[0-9]+)/(?P<action>\w+)$',
                 'simple_rating.views.vote',
                 name='vote'),
             )
