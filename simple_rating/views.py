# Create your views here.
from django.db.models import get_model
from django.http import HttpResponse, \
    HttpResponseNotFound, \
    HttpResponseBadRequest
from django.contrib.auth import get_user


def vote(request, appname, model_name, id, action):
    if request.method != "POST":
        return HttpResponseBadRequest("Method not allowed")
    try:
        model = get_model(appname, model_name)
        if model is None:
            raise ValueError("No such model {0}.{1}".format(
                appname, model_name))
        try:
            inst = model.objects.filter(id=int(id))[0]
        except IndexError:
            raise ValueError("No such instance {0}".format(id))
        multis = {
            "minus": -1,
            "plus": 1
        }
        inst.vote(request.user, 1 * multis[action])
        return HttpResponse(inst.get_total_rate())
    except ValueError as err:
        return HttpResponseNotFound(str(err))
