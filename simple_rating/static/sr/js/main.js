(function() {

    Voter = function(element, increase, token) {
        this.element = element;
        this.buttons = element.parent().find(".sr_voter")
        this.counter = element.parent().parent().find(".stats .digit");
        this.url = element.attr("href");
        this.errorHolder = element.parent().find(".errors")
        this.state = 0;
        this.token = token;
        if (increase) {
            this.action = "plus";
        }
        else {
            this.action = "minus";
        }
    }

    Voter.prototype.vote = function() {
        if (!this.state) {
            this.state = 1;
            var self = this;
            console.log(self.url)
            var requestObject = $.ajax(this.url,
                {
                    "method": "POST",
                    "beforeSend": function (request) {
                        request.setRequestHeader("X-CSRFToken", self.token);
                     },
                }
            );
            requestObject.done(function(answer) {
                console.log(answer)
                self.counter.html(answer);
                self.buttons.addClass("enable");
                self.buttons.removeClass("disable");

                self.element.addClass("disable");
                self.element.removeClass("enable");
            });
            requestObject.fail(function(error) {
                self.errorHolder.html("Error happened");
            });
            requestObject.always(function() {
                self.state = 0;
            });
        }
    }

    $(document).ready(function() {
        $("a.sr_voter").on("click", function(e) {
            var element = $(e.target);
            var increase = element.hasClass("sr_plus");
            var enabled = element.hasClass("enable");
            var token = $.cookie("csrftoken");
            if (enabled) {
                var voter = new Voter(element, increase, token);
                voter.vote();
            }
            e.preventDefault();
        });
    });
})();