from setuptools import setup, find_packages

print find_packages()

setup(
    name='Django Simple Rating',
    version='0.1',
    packages=find_packages(),
    url='https://bitbucket.org/eith_sp/simple_rating',
    license='BSD',
    author='Michael Sahnov',
    author_email='sahnov.m@gmail.com',
    description='',
    install_requires=['django >= 1.3']
)
